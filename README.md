# Loom Playground

Playground for the Loom language.

Try it out at: https://loom-lang.gitlab.io/loom-playground

## Installation

Install all dependencies:
```sh
npm install
```

Build the application:
```
npm run build
```

Open `index.html`:
```
npm run open
```